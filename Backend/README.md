# CarPool

This is a project for a car pooling application.

Trello board:
https://trello.com/b/gJvdDFGt/carpool-project

Database diagram:
![Diagram](src/main/resources/static/carpool_database.png)
package com.shipcars.finalproject.mockobjects;

import com.shipcars.finalproject.entities.Role;
import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.CreateUserDTO;
import com.shipcars.finalproject.models.EditUserDTO;
import com.shipcars.finalproject.models.LoginDTO;
import com.shipcars.finalproject.models.UserDTO;
import com.shipcars.finalproject.repositories.UserRepository;
import com.shipcars.finalproject.security.JwtTokenProvider;
import com.shipcars.finalproject.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private PasswordEncoder mockPasswordEncoder;

    @Mock
    private AuthenticationManager mockAuthenticationManager;

    @Mock
    private JwtTokenProvider mockJwtTokenProvider;

    @Mock
    private ModelMapper mockModelMapper;

    @InjectMocks
    private UserServiceImpl userService;

    List<Role> roles = new ArrayList<>(Arrays.asList(Role.ROLE_USER));
    private User user = new User(1, "pesh", "pesho", "petrov", "pesho@test.com",
            "12345678", "0888888888", 0.0, 0.0, null, roles);
    private CreateUserDTO createUserDTO = new CreateUserDTO("pesh", "pesho", "petrov",
            "pesho@test.com", "password", "0888888888");
    private UserDTO userDTO = new UserDTO(1l, "pesh", "pesho", "petrov",
            "pesho@test.com", "0888888888", 2, 2, null);
    private LoginDTO loginDTO = new LoginDTO("pesho@test.com", "password", true);
    private EditUserDTO editUserDTO = new EditUserDTO((long) 1, "pesh", "pesho", "petrov",
            "pesho@test.com", "0888888888");
    List<User> users = Arrays.asList();

    @Test
    public void createUserShouldCallSave() {
        when(mockModelMapper.map(createUserDTO, User.class)).thenReturn(user);

        userService.createUser(createUserDTO);

        verify(mockUserRepository, times(1)).save(user);
    }

    @Test
    public void createUserShouldGenerateCustomUsername() {
        String emailPrefix = user.getEmail().split("@")[0];
        when(mockModelMapper.map(createUserDTO, User.class)).thenReturn(user);
        when(mockUserRepository.findAllByUsernameContaining(emailPrefix)).thenReturn(Arrays.asList(user));

        userService.createUser(createUserDTO);

        verify(mockUserRepository, times(1)).save(user);
    }

    @Test(expected = CustomException.class)
    public void createUserShouldThrowExceptionWhenUserDoesNotExist() {
        userService.createUser(null);
    }

    @Test(expected = CustomException.class)
    public void createUserShouldThrowExceptionWhenUserAlreadyExists() {
        when(mockUserRepository.findAllByEmail(createUserDTO.getEmail())).thenReturn(user);

        userService.createUser(createUserDTO);
    }

    @Test
    public void loginShouldLogin() {
        when(mockUserRepository.findAllByEmail(loginDTO.getEmail())).thenReturn(user);

        userService.authorize(loginDTO);

        Mockito.verify(mockUserRepository, Mockito.times(1))
               .findAllByEmail(loginDTO.getEmail());
    }

    @Test
    public void loginShouldCallAuthenticationManager() {
        when(mockUserRepository.findAllByEmail(loginDTO.getEmail())).thenReturn(user);

        userService.authorize(loginDTO);

        Mockito.verify(mockAuthenticationManager, Mockito.times(1))
               .authenticate(new UsernamePasswordAuthenticationToken("pesh",
                       loginDTO.getPassword()));
    }

    @Test(expected = CustomException.class)
    public void loginShouldThrowExceptionWhenUserDoesNotExist() {
        when(mockUserRepository.findAllByEmail(loginDTO.getEmail())).thenReturn(null);

        userService.authorize(loginDTO);
    }

    @Test(expected = CustomException.class)
    public void loginShouldThrowExceptionWhenCredentialsDontMatch() {
        when(mockUserRepository.findAllByEmail(loginDTO.getEmail())).thenReturn(user);
        when(mockAuthenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken("pesh", "password")))
                .thenThrow(new AuthenticationCredentialsNotFoundException(""));

        userService.authorize(loginDTO);
    }

    @Test
    public void getCurrentUserShouldReturnUser() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockUserRepository
                .findByUsername(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))))
                .thenReturn(user);

        mockModelMapper.map(userService.getCurrentUser(req), User.class);

        Assert.assertEquals("pesh", user.getUsername());
    }

    @Test
    public void refreshShouldRefresh() {
        when(mockUserRepository.findByUsername("pesh")).thenReturn(user);

        userService.refresh(user.getUsername());

        verify(mockJwtTokenProvider, times(1)).createToken(user.getUsername(), roles);
    }

    @Test
    public void editUserShouldEdit() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockUserRepository.findByUsername(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))))
                .thenReturn(user);
        when(mockModelMapper.map(user, EditUserDTO.class)).thenReturn(editUserDTO);

        userService.editUser(editUserDTO, req);

        verify(mockUserRepository, times(1)).save(user);
    }

    @Test(expected = CustomException.class)
    public void editUserShouldThrowExceptionWhenUserDoesntExists() {
        HttpServletRequest req = mock(HttpServletRequest.class);

        userService.editUser(null, req);
    }

    @Test
    public void getByUsernameShouldReturnUser() {
        when(mockUserRepository.findByUsername("pesh")).thenReturn(user);

        userService.getUserByUsername("pesh");

        verify(mockUserRepository, times(1)).findByUsername("pesh");
    }

    @Test(expected = CustomException.class)
    public void getByUsernameShouldThrowExceptionWhenUserDoesntExists() {
        when(mockUserRepository.findByUsername("pesh")).thenReturn(null);

        userService.getUserByUsername("pesh");
    }

    @Test(expected = CustomException.class)
    public void getByUsernameShouldThrowExceptionWhenUsernameDoesntExists() {
        userService.getUserByUsername(null);
    }

    @Test
    public void getAllUsersShouldReturnUser() {
        userService.getAllUsers(user.getUsername());

        Mockito.verify(mockUserRepository, times(1))
               .findAllByUsernameContaining(user.getUsername());
    }

    @Test
    public void getAllUsersShouldReturnUsers() {
        when(mockUserRepository.findAll()).thenReturn(users);

        userService.getAllUsers(null);

        verify(mockUserRepository, times(1)).findAll();
    }

    @Test
    public void getUserImageByUserIdShouldReturnImage() {
        String idString = "1";
        when(mockUserRepository.findUserById(Long.parseLong(idString))).thenReturn(user);

        userService.getUserImageByUserId(idString);

        verify(mockUserRepository, times(1)).findUserById(Long.parseLong(idString));
    }
}



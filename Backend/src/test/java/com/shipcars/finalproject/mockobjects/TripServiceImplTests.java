package com.shipcars.finalproject.mockobjects;

import com.shipcars.finalproject.entities.*;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.CreateCommentDTO;
import com.shipcars.finalproject.models.CreateTripDTO;
import com.shipcars.finalproject.models.EditTripDTO;
import com.shipcars.finalproject.models.UserDTO;
import com.shipcars.finalproject.models.statuses.PassengerStatus;
import com.shipcars.finalproject.models.statuses.TripStatus;
import com.shipcars.finalproject.repositories.*;
import com.shipcars.finalproject.security.JwtTokenProvider;
import com.shipcars.finalproject.services.TripServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class TripServiceImplTests {

    @Mock
    private TripRepository mockTripRepository;

    @Mock
    private CommentRepository mockCommentRepository;

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private ModelMapper mockModelMapper;

    @Mock
    private JwtTokenProvider mockJwtTokenProvider;

    @Mock
    private PassengerRepository mockPassengerRepository;

    @Mock
    private UserRatingRepository mockUserRatingRepository;

    @InjectMocks
    private TripServiceImpl tripService;

    private List<Role> roles = new ArrayList<>(Arrays.asList(Role.ROLE_USER));
    private User user = new User(1, "pesh", "pesho", "petrov", "pesho@test.com",
            "12345678", "0888888888", 0.0, 0.0, null, roles);
    private User user2 = new User(2, "pesh2", "pesho", "petrov", "pesho@test.com",
            "12345678", "0888888888", 0.0, 0.0, null, roles);
    private LocalDateTime departureTime = LocalDateTime.parse("2019-09-02T11:17:59");
    private Passenger passenger = new Passenger(1, 1, user2, PassengerStatus.pending);

    private Comment comment = new Comment(1, "message", 1, user);
    private List<Comment> comments = Arrays.asList(comment);
    private List<Passenger> passengers = Arrays.asList(passenger);
    private List<User> users = Arrays.asList(user2);
    private PassengerStatus passengerStatus = PassengerStatus.accepted;
    private Long id = 1L;
    private Integer availablePlaces = 4;

    private Trip trip = new Trip(1, user, "Ford", "message", departureTime, "Sliven", "Sofia", availablePlaces,
            passengers,
            TripStatus.available, comments, true, true, true);

    private Trip trip2 = new Trip(1, user2, "Ford", "message", departureTime, "Sliven", "Sofia", availablePlaces,
            passengers,
            TripStatus.available, comments, true, true, true);

    private List<Trip> trips = Arrays.asList(trip);


    private UserDTO userDTO = new UserDTO(id, "pesh", "pesho", "petrov", "pesho@test.com",
            "0888888888", 0.0, 0.0, null);

    private CreateCommentDTO commentDTO = new CreateCommentDTO("message", userDTO);

    private EditTripDTO editTripDTO = new EditTripDTO(id, "Ford", "message", departureTime, "Sliven", "Sofia", 4, true,
            true, true);

    private CreateTripDTO createTripDTO = new CreateTripDTO("Ford", "message", departureTime, "Sliven", "Sofia", 4,
            true,
            true,
            true);

    private MockHttpServletRequest req = new MockHttpServletRequest();

    private UserTripRating driver = new UserTripRating(1, user2.getId(), 4, "driver", 1, 2);

    @Test
    public void createTripShouldCreateTrip() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockUserRepository
                .findByUsername(mockJwtTokenProvider
                        .getUsername(mockJwtTokenProvider
                                .resolveToken(req))))
                .thenReturn(user);
        when(mockModelMapper.map(createTripDTO, Trip.class)).thenReturn(trip);
        trip.setDriver(user);
        tripService.createTrip(createTripDTO, req);
        verify(mockTripRepository, times(1)).save(trip);
    }

    @Test
    public void addCommentShouldAddComment() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        when(mockUserRepository.findByUsername("pesh"))
                .thenReturn(user);

        tripService.addComment(id, commentDTO, req);
        verify(mockCommentRepository, times(1)).save(isA(Comment.class));
    }

    @Test(expected = CustomException.class)
    public void addCommentShouldThrowExceptionWhenTripDoesNotExist() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(new ArrayList<>());

        tripService.addComment(id, commentDTO, req);
    }

    @Test(expected = CustomException.class)
    public void addCommentShouldThrowExceptionWhenIdEqualsNull() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(null))).thenReturn(null);

        tripService.addComment(null, commentDTO, req);
    }

    @Test(expected = CustomException.class)
    public void addCommentShouldThrowExceptionWhenUserNotDriver() {
        List<Trip> trips3 = new ArrayList<>();
        trips3.add(trip);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips3);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))).thenReturn("pesh2");
        when(mockUserRepository.findByUsername("pesh2")).thenReturn(user2);
        User user1 = trips3.get(0).getDriver();
        tripService.addComment(id, commentDTO, req);
    }

    @Test(expected = CustomException.class)
    public void addCommentShouldThrowExceptionWhenUserNotPassenger() {
        List<Trip> trips3 = new ArrayList<>();
        trips3.add(trip2);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips3);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))).thenReturn("pesh");
        when(mockUserRepository.findByUsername("pesh")).thenReturn(user);

        User user1 = trips3.get(0).getDriver();
        tripService.addComment(id, commentDTO, req);
    }

    @Test
    public void editTripCallsTripRepositorySave() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(editTripDTO.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        when(mockModelMapper.map(trips.get(0), EditTripDTO.class)).thenReturn(editTripDTO);
        tripService.editTrip(mockModelMapper.map(trips.get(0), EditTripDTO.class), req);
        verify(mockTripRepository, times(1)).save(trip);
    }

    @Test(expected = CustomException.class)
    public void editTripThrowsExceptionWhenTripDoesNotExist() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(editTripDTO.getId()))).thenReturn(new ArrayList<>());
        tripService.editTrip(editTripDTO, req);
    }

    @Test(expected = CustomException.class)
    public void editTripShouldThrowExceptionWhenUsersNotDriver() {
        List<Trip> trips3 = new ArrayList<>();
        trips3.add(trip);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips3);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))).thenReturn("pesh2");
        User user1 = trips3.get(0).getDriver();

        tripService.editTrip(editTripDTO, req);
    }

    @Test(expected = CustomException.class)
    public void editTripShouldThrowExceptionWhenNotEnoughPlaces() {
        List<Trip> trips3 = new ArrayList<>();
        trips3.add(trip);
        trip.setPassengersList(Arrays.asList(passenger, passenger, passenger, passenger, passenger));
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips3);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))).thenReturn("pesh");

        tripService.editTrip(editTripDTO, req);
    }

    @Test
    public void changeTripStatusShouldChange() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        tripService.changeTripStatus(1, TripStatus.booked, req);
        Assert.assertEquals(TripStatus.booked, trips.get(0).getStatus());
    }

    @Test
    public void changeTripStatusShouldCallSave() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        tripService.changeTripStatus(1, TripStatus.booked, req);
        verify(mockTripRepository, times(1)).save(trip);
    }

    @Test
    public void changeTripStatusShouldCallDeleteWhenStatusIsCanceled() {
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        tripService.changeTripStatus(id, TripStatus.canceled, req);

        verify(mockTripRepository, times(1)).deleteById(id);
        verify(mockPassengerRepository, times(1)).deleteAllByTripId(id);
    }

    @Test(expected = CustomException.class)
    public void changeTripStatusShouldThrowExceptionWhenTripDoesntExists() {
        HttpServletRequest req = mock(HttpServletRequest.class);

        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(new ArrayList<>());
        tripService.changeTripStatus(1, TripStatus.booked, req);
    }

    @Test(expected = CustomException.class)
    public void changeTripStatusShouldThrowExceptionWhenSkippingStatus() {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");

        tripService.changeTripStatus(1, TripStatus.ongoing, req);
    }

    @Test(expected = CustomException.class)
    public void changeTripStatusShouldThrowExceptionWhenNotDriver() {
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))).thenReturn("pesh2");
        tripService.changeTripStatus(1, TripStatus.booked, req);
    }

    @Test
    public void changePassengerStatusShouldChange() {
        trips.get(0).getPassengersList().get(0).setStatus(passengerStatus);
        Assert.assertEquals(PassengerStatus.accepted, trips.get(0).getPassengersList().get(0).getStatus());
    }

    @Test(expected = CustomException.class)
    public void changePassengerStatusShouldThrowExceptionWhenTripDoesNotExist() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId())))
                .thenReturn(new ArrayList<>());

        tripService.changePassengerStatus(1, 1, PassengerStatus.absent);
    }

    @Test(expected = CustomException.class)
    public void changePassengerStatusShouldThrowExceptionWhenPassengerDoesNotExist() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        tripService.changePassengerStatus(1, -1, PassengerStatus.accepted);
    }

    @Test
    public void changePassengerStatusShouldCallDeleteWhenStatusIsRejected() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockUserRepository.findUserById(id)).thenReturn(user);
        when(mockPassengerRepository.findAllByTripIdAndUser(id, user)).thenReturn(passenger);

        trips.get(0).getPassengersList().get(0).setStatus(passengerStatus);

        tripService.changePassengerStatus(id, id, PassengerStatus.rejected);

        verify(mockPassengerRepository, times(1)).delete(passenger);
    }

    @Test
    public void changePassengerStatusShouldCallDeleteWhenStatusIsNotRejectedOrCanceled() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockUserRepository.findUserById(id)).thenReturn(user);
        when(mockPassengerRepository.findAllByTripIdAndUser(id, user)).thenReturn(passenger);

        trips.get(0).getPassengersList().get(0).setStatus(passengerStatus);

        tripService.changePassengerStatus(id, id, PassengerStatus.accepted);

        verify(mockPassengerRepository, times(1)).save(passenger);
    }

    @Test
    public void allPassengerStatusesShouldReturnStatuses() {
        List<PassengerStatus> statuses;
        ArrayList<PassengerStatus> statusesToCheck = new ArrayList<>(Arrays.asList(PassengerStatus.values()));

        statuses = tripService.allPassengerStatuses();

        Assert.assertArrayEquals(statusesToCheck.toArray(), statuses.toArray());
    }

    @Test
    public void allTripStatusesShouldReturnStatuses() {
        List<TripStatus> statuses;
        ArrayList<TripStatus> statusesToCheck = new ArrayList<>(Arrays.asList(TripStatus.values()));

        statuses = tripService.allTripStatuses();

        Assert.assertArrayEquals(statusesToCheck.toArray(), statuses.toArray());
    }

    @Test
    public void applyShouldApply() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername("pesh2"))
                .thenReturn(user);

        tripService.addPassenger(trip.getId(), req);
        verify(mockPassengerRepository, times(1)).save(isA(Passenger.class));
    }

    @Test(expected = CustomException.class)
    public void applyShouldThrowExceptionWhenTripDoesNotExist() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(new ArrayList<>());
        tripService.addPassenger(trip.getId(), req);
        verify(mockPassengerRepository, times(1)).save(isA(Passenger.class));
    }

    @Test(expected = CustomException.class)
    public void applyShouldThrowExceptionWhenUserIsDriver() {
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req))).thenReturn("pesh");

        tripService.addPassenger(trip.getId(), req);
    }

    @Test(expected = CustomException.class)
    public void applyShouldThrowExceptionWhenUserNotFound() {
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername("pesh2"))
                .thenReturn(null);

        tripService.addPassenger(trip.getId(), req);
    }

    @Test(expected = CustomException.class)
    public void applyShouldThrowExceptionWhenUserAlreadyApplied() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername("pesh2"))
                .thenReturn(user);
        when(mockPassengerRepository.findAllByTripIdAndUser(1, user)).thenReturn(passenger);
        tripService.addPassenger(trip.getId(), req);
    }

    @Test(expected = CustomException.class)
    public void applyShouldThrowExceptionWhenTripStatusIsNotAvailable() {
        when(mockTripRepository.findAllById(Collections.singleton(id))).thenReturn(trips);
        trips.get(0).setStatus(TripStatus.booked);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername("pesh2"))
                .thenReturn(user);
        tripService.addPassenger(trip.getId(), req);
    }

    @Test(expected = CustomException.class)
    public void applyShouldThrowExceptionWhenNoAvailablePlaces() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        trips.get(0).setAvailablePlaces(trips.get(0).getPassengersList().size());

        when(mockUserRepository.findByUsername("pesh2"))
                .thenReturn(user);

        tripService.addPassenger(trip.getId(), req);
    }

    @Test
    public void rateDriverShouldRate() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername(user2.getUsername()))
                .thenReturn(user2);

        when(mockPassengerRepository.findAllByTripIdAndUser(trips.get(0).getId(), user2))
                .thenReturn(passenger);
        when(mockUserRatingRepository.findByUserIdAndTripIdAndRatedBy
                (trips.get(0).getDriver().getId(), trips.get(0).getId(), user2.getId()))
                .thenReturn(driver);
        tripService.rateDriver(trip.getId(), 4, req);
        verify(mockUserRatingRepository, times(1)).save(isA(UserTripRating.class));
    }

    @Test(expected = CustomException.class)
    public void rateDriverShouldThrowExceptionWhenTripDoesNotExist() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(new ArrayList<>());
        tripService.rateDriver(trip.getId(), 4, req);
    }

    @Test(expected = CustomException.class)
    public void rateDriverShouldThrowExceptionWhenTripWhichIsNotDone() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        trips.get(0).setStatus(TripStatus.ongoing);
        tripService.rateDriver(trip.getId(), 4, req);
    }

    @Test(expected = CustomException.class)
    public void rateDriverShouldThrowExceptionWhenPassengerNotOnTheTrip() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername(user2.getUsername()))
                .thenReturn(user2);

        tripService.rateDriver(trip.getId(), 4, req);
    }

    @Test(expected = CustomException.class)
    public void rateDriverShouldThrowExceptionWhenPassengerNotAuthorized() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername(user2.getUsername()))
                .thenReturn(user2);

        when(mockPassengerRepository.findAllByTripIdAndUser(trips.get(0).getId(), user2))
                .thenReturn(null);
        tripService.rateDriver(trip.getId(), 4, req);
    }

    @Test
    public void rateDriverShouldRateWhenRatingDoesNotExist() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername(user2.getUsername()))
                .thenReturn(user2);

        when(mockPassengerRepository.findAllByTripIdAndUser(trips.get(0).getId(), user2))
                .thenReturn(passenger);
        tripService.rateDriver(trip.getId(), 4, req);
        verify(mockUserRatingRepository, times(1)).save(isA(UserTripRating.class));
    }

    @Test
    public void ratePassengerShouldRate() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        when(mockUserRepository.findByUsername(user.getUsername()))
                .thenReturn(user);

        when(mockUserRepository.findAllById(Collections.singleton(passenger.getId()))).thenReturn(users);

        when(mockPassengerRepository.findAllByTripIdAndUser(trips.get(0).getId(), user2))
                .thenReturn(passenger);
        when(mockUserRatingRepository.findByUserIdAndTripIdAndRatedBy
                (trips.get(0).getDriver().getId(), trips.get(0).getId(), user.getId()))
                .thenReturn(driver);
        tripService.ratePassenger(trips.get(0).getId(), passenger.getId(), 4, req);
        verify(mockUserRatingRepository, times(1)).save(isA(UserTripRating.class));
    }

    @Test(expected = CustomException.class)
    public void ratePassengerShouldThrowExceptionWhenTripIsNotDone() {
        trips.get(0).setStatus(TripStatus.available);
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        tripService.ratePassenger(trips.get(0).getId(), passenger.getId(), 4, req);
    }

    @Test(expected = CustomException.class)
    public void ratePassengerShouldThrowExceptionWhenTripDoesNotExist() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(new ArrayList<>());
        tripService.ratePassenger(trips.get(0).getId(), passenger.getId(), 4, req);
    }

    @Test(expected = CustomException.class)
    public void ratePassengerShouldThrowExceptionWhenDriverNotAuthorizedToRatePassengers() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh2");
        when(mockUserRepository.findByUsername(user2.getUsername()))
                .thenReturn(user2);

        tripService.ratePassenger(trips.get(0).getId(), passenger.getId(), 4, req);
    }

    @Test(expected = CustomException.class)
    public void ratePassengerShouldThrowExceptionWhenDriverNotAuthorized() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        when(mockUserRepository.findByUsername(user.getUsername()))
                .thenReturn(user);

        when(mockUserRepository.findAllById(Collections.singleton(passenger.getId()))).thenReturn(users);

        when(mockPassengerRepository.findAllByTripIdAndUser(trips.get(0).getId(), user2))
                .thenReturn(null);

        tripService.ratePassenger(trips.get(0).getId(), passenger.getId(), 4, req);
    }

    @Test(expected = CustomException.class)
    public void ratePassengerShouldThrowExceptionWhenPassengerNotAuthorized() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        when(mockUserRepository.findByUsername(user.getUsername()))
                .thenReturn(user);

        when(mockUserRepository.findAllById(Collections.singleton(passenger.getId()))).thenReturn(users);
        when(mockPassengerRepository.findAllByTripIdAndUser(trips.get(0).getId(), users.get(0))).thenReturn(null);

        tripService.ratePassenger(trips.get(0).getId(), passenger.getId(), 4, req);
    }

    @Test
    public void ratePassengerShouldRateWhenRatingDoesNotExist() {
        trips.get(0).setStatus(TripStatus.done);

        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        when(mockJwtTokenProvider.getUsername(mockJwtTokenProvider.resolveToken(req)))
                .thenReturn("pesh");
        when(mockUserRepository.findByUsername(user.getUsername()))
                .thenReturn(user);

        when(mockUserRepository.findAllById(Collections.singleton(passenger.getId()))).thenReturn(users);

        when(mockPassengerRepository.findAllByTripIdAndUser(trips.get(0).getId(), user2))
                .thenReturn(passenger);
        tripService.ratePassenger(trips.get(0).getId(), passenger.getId(), 4, req);
        verify(mockUserRatingRepository, times(1)).save(isA(UserTripRating.class));
    }
}
package com.shipcars.finalproject.mockobjects;

import com.shipcars.finalproject.entities.Comment;
import com.shipcars.finalproject.entities.Passenger;
import com.shipcars.finalproject.entities.Trip;
import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.statuses.PassengerStatus;
import com.shipcars.finalproject.models.statuses.TripStatus;
import com.shipcars.finalproject.models.UserDTO;
import com.shipcars.finalproject.repositories.PassengerRepository;
import com.shipcars.finalproject.repositories.TripRepository;
import com.shipcars.finalproject.repositories.UserRepository;
import com.shipcars.finalproject.services.TripRepresentationServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TripRepresentationServiceImplTests {

    @Mock
    private TripRepository mockTripRepository;

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private ModelMapper mockModelMapper;

    @Mock
    private PassengerRepository mockPassengerRepository;

    @InjectMocks
    private TripRepresentationServiceImpl tripRepresentationService;

    private DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    private LocalDateTime earliest = LocalDateTime.parse("1900-01-01T00:00:00", formatter);
    private LocalDateTime latest = LocalDateTime.parse("2900-01-01T00:00:00", formatter);
    private LocalDateTime departureTime = LocalDateTime.parse("2019-09-15T00:00:00", formatter);
    private User driver = new User(1, "pesh", "pesho", "petrov", "pesho@test.com",
            "12345678", "0888888888", 0.0, 0.0, null, null);
    private Trip tripOne = new Trip(1, driver, "Porshe", null, null, "Sofia",
            "Burgas", 3, new ArrayList<>(), TripStatus.available, new ArrayList<>(), true,
            true, true);
    private List<TripStatus> tripStatuses = new ArrayList<>(Arrays.asList(TripStatus.values()));
    private Set<Long> driversId = new HashSet<>(Arrays.asList(1l));
    private List<User> drivers = new ArrayList<>(Arrays.asList(driver));
    private List<Trip> trips = new ArrayList<>(Arrays.asList(tripOne));
    private List<Integer> availablePlaces = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    private List<Boolean> smoking = new ArrayList<>(Arrays.asList(true, false));
    private List<Boolean> pets = new ArrayList<>(Arrays.asList(true, false));
    private List<Boolean> luggage = new ArrayList<>(Arrays.asList(true, false));


    @Test
    public void getTripsShouldReturnTripNoFiltersApplied() {
        when(mockTripRepository.getDrivers())
                .thenReturn(driversId);
        when(mockUserRepository.findAllById(driversId))
                .thenReturn(drivers);
        Page<Trip> page = new PageImpl<>(trips);
        when(mockTripRepository
                .findAllByStatusInAndDriverInAndOriginContainingAndDestinationContainingAndDepartureTimeBetweenAndAvailablePlacesInAndSmokingInAndPetsInAndLuggageIn
                        (tripStatuses, drivers, "", "", earliest, latest, availablePlaces,
                                smoking, pets, luggage, null)).thenReturn(page);

        tripRepresentationService.getTrips(null, null, null, null, null,
                null, null, null, null, null, null);
        Assert.assertEquals(trips.size(), page.getContent().size());
        Assert.assertEquals(trips.get(0), page.getContent().get(0));
        Assert.assertEquals(trips.get(0).getId(), page.getContent().get(0).getId());
    }

    @Test
    public void getTripsShouldReturnTripFiltersApplied() {
        UserDTO driverDTO = new UserDTO(driver.getId(), driver.getUsername(), driver.getFirstName(),
                driver.getLastName(), driver.getEmail(), driver.getPhone(), driver.getRatingAsDriver(),
                driver.getRatingAsPassenger(), driver.getImage());
        User userTwo = new User(2, "gosho", "gosho", "first", "gosho@test.com",
                "12345678", "0888123456", 0.0, 0.0, null, null);
        Passenger passenger = new Passenger(1, 1, userTwo, PassengerStatus.pending);
        Comment comment = new Comment(1, "message", 1, driver);
        Trip tripTwo = new Trip(2, driver, "Trabant", "Free ride", departureTime, "Sofia",
                "Burgas", 3, new ArrayList<>(Arrays.asList(passenger)), TripStatus.available,
                new ArrayList<>(Arrays.asList(comment)), false, true, true);
        trips.add(tripTwo);
        when(mockUserRepository.findByUsername(driver.getUsername()))
                .thenReturn(driver);
        Page<Trip> page = new PageImpl<>(trips);
        when(mockTripRepository
                .findAllByStatusInAndDriverInAndOriginContainingAndDestinationContainingAndDepartureTimeBetweenAndAvailablePlacesInAndSmokingInAndPetsInAndLuggageIn
                        (Collections.singletonList(TripStatus.available), Collections.singletonList(driver), "Sofia",
                                "Burgas",
                                earliest, latest, Collections.singletonList(3), Collections.singletonList(false),
                                Collections.singletonList(true), Collections.singletonList(true), null))
                .thenReturn(page);

        when(mockModelMapper.map(tripOne.getDriver(), UserDTO.class))
                .thenReturn(driverDTO);

        tripRepresentationService.getTrips(TripStatus.available, driver.getUsername(), "Sofia", "Burgas",
                "1900-01-01T00:00:00", "2900-01-01T00:00:00", 3,
                false, true, true, null);
        Assert.assertEquals(trips.size(), page.getContent().size());
        Assert.assertEquals(trips.get(0), page.getContent().get(0));
        Assert.assertEquals(trips.get(0).getId(), page.getContent().get(0).getId());
    }

    @Test
    public void getTripShouldReturnTrip() {
        when(mockTripRepository.findAllById(Collections.singleton(tripOne.getId()))).thenReturn(trips);
        tripRepresentationService.getTrip(tripOne.getId());
        Mockito.verify(mockTripRepository, Mockito.times(1)).findAllById(Collections.singleton(tripOne.getId()));
    }

    @Test(expected = CustomException.class)
    public void getTripByIDShouldThrowExceptionWhenTripDoesntExists() {
        tripRepresentationService.getTrip(tripOne.getId());
    }

    @Test(expected = CustomException.class)
    public void getTripsShouldThrowExceptionWhenDriverDoesntExists() {
        tripRepresentationService.getTrips(null, driver.getUsername(), null, null, null,
                null, null, null, null, null, null);
    }
}
package com.shipcars.finalproject.mockobjects;

import com.shipcars.finalproject.entities.*;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.statuses.PassengerStatus;
import com.shipcars.finalproject.models.statuses.TripStatus;
import com.shipcars.finalproject.repositories.TripRepository;
import com.shipcars.finalproject.repositories.UserRepository;
import com.shipcars.finalproject.services.AdminServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceImplTests {

    @Mock
    private TripRepository mockTripRepository;

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private AdminServiceImpl adminService;

    private Role role = Role.ROLE_ADMIN;
    private List<Role> roles = new ArrayList<>(Arrays.asList(Role.ROLE_ADMIN));
    private User user = new User(1, "pesh", "pesho", "petrov", "pesho@test.com",
            "12345678", "0888888888", 0.0, 0.0, null, roles);
    private LocalDateTime departureTime = LocalDateTime.parse("2019-09-02T11:17:59");
    private Comment comment = new Comment(1, "message", 1, user);
    private List<Comment> comments = Arrays.asList(comment);
    private Passenger passenger = new Passenger(1, 1, user, PassengerStatus.pending);
    private List<Passenger> passengers = Arrays.asList(passenger);
    private Trip trip = new Trip(1, user, "Ford", "message", departureTime, "Sliven", "Sofia", 2,
            passengers,
            TripStatus.available, comments, true, true, true);
    private List<Trip> trips = Arrays.asList(trip);

    @Test
    public void deleteUserShouldDelete() {
        when(mockUserRepository.findUserById(user.getId())).thenReturn(user);
        adminService.deleteUser(user.getId());

        verify(mockUserRepository, times(1)).delete(user);
    }

    @Test(expected = CustomException.class)
    public void deleteUserShouldThrowExceptionWhenUserDoesNotExist() {
        when(mockUserRepository.findUserById(user.getId())).thenReturn(null);
        adminService.deleteUser(user.getId());
    }

    @Test
    public void deleteTripShouldDelete() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(trips);
        adminService.deleteTrip(trip.getId());

        verify(mockTripRepository, times(1)).delete(trip);
    }

    @Test(expected = CustomException.class)
    public void deleteTripShouldThrowExceptionWhenTripDoesNotExist() {
        when(mockTripRepository.findAllById(Collections.singleton(trip.getId()))).thenReturn(new ArrayList<>());
        adminService.deleteTrip(trip.getId());
    }

    @Test
    public void editUserRoleShouldEdit() {
        user.setRoles(new ArrayList<>(Arrays.asList(role)));

        Assert.assertEquals(Role.ROLE_ADMIN, user.getRoles().get(0));
    }

    @Test
    public void editUserRoleShouldCallSave() {
        when(mockUserRepository.findByUsername("pesh")).thenReturn(user);
        adminService.editUserRole("pesh", role);
        verify(mockUserRepository, times(1)).save(user);
    }

    @Test(expected = CustomException.class)
    public void editUserRoleShouldThrowExceptionWhenUserDoesNotExist() {
        when(mockUserRepository.findByUsername("pesh")).thenReturn(null);

        adminService.editUserRole("pesh", role);
    }
}

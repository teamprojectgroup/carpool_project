package com.shipcars.finalproject.mockobjects;

import com.shipcars.finalproject.entities.Role;
import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.entities.UserImage;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.repositories.UserImageRepository;
import com.shipcars.finalproject.repositories.UserRepository;
import com.shipcars.finalproject.services.UserImageServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserImageServiceImplTests {
    @Mock
    private UserImageRepository mockUserImageRepository;

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private UserImageServiceImpl userImageService;

    private List<Role> roles = new ArrayList<>(Arrays.asList(Role.ROLE_USER));
    private Long id = 1L;
    private User user = new User(id, "pesh", "pesho", "petrov", "pesho@test.com",
            "12345678", "0888888888", 0.0, 0.0, null, roles);
    private UserImage userImage = new UserImage("Image", 1, "Type", null);

    @Test
    public void storeFileShouldCallRepositorySave() {
        when(mockUserRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(user));
        when(mockUserImageRepository.save(userImage)).thenReturn(userImage);
        userImageService.storeFile(userImage, String.valueOf(1));

        verify(mockUserImageRepository, times(1)).save(userImage);
    }

    @Test(expected = CustomException.class)
    public void storeFileShouldThrowExceptionWhenUserDoesNotExists() {
        userImageService.storeFile(userImage, String.valueOf(1));

        verify(mockUserImageRepository, Mockito.never()).save(userImage);
    }

    @Test
    public void getFileShouldReturnUserImageWhenIdExists() {
        when(mockUserRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(user));
        when(mockUserImageRepository.getUserImageByUserId(1))
                .thenReturn(userImage);

        UserImage image = userImageService.getFile(String.valueOf(1));

        Assert.assertEquals(0, image.getId());
        Assert.assertEquals("Image", image.getFileName());
    }


    @Test(expected = CustomException.class)
    public void getFileShouldThrowExceptionWhenIdDoesNotExists() {
        userImageService.getFile(String.valueOf(1));

        verify(mockUserImageRepository, never()).findById(1);
    }
}
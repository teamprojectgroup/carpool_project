package com.shipcars.finalproject.models.statuses;

public enum TripStatus {
    available, booked, ongoing, done, canceled
}

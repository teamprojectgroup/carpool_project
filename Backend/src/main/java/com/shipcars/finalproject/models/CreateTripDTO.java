package com.shipcars.finalproject.models;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

public class CreateTripDTO {

    @Size(min = 3, max = 50, message = "Car model should be between 3 and 50!")
    @Column(name = "car_model")
    private String carModel;

    @Column(name = "message")
    @Size(max = 255, message = "Message cannot be more than 255 symbols!")
    private String message;

    @Column(name = "departure_time")
    private LocalDateTime departureTime;

    @Column(name = "origin")
    @Size(min = 3, max = 50, message = "Origin should be between 3 and 50!")
    private String origin;

    @Column(name = "destination")
    @Size(min = 3, max = 50, message = "Destination should be between 3 and 50!")
    private String destination;

    @Column(name = "available_places")
    @NotNull
    @Min(1)
    @Max(10)
    private int availablePlaces;

    @Column(name = "smoking")
    private boolean smoking;

    @Column(name = "pets")
    private boolean pets;

    @Column(name = "luggage")
    private boolean luggage;

    public CreateTripDTO() {
    }

    public CreateTripDTO(String carModel, String message, LocalDateTime departureTime
            , String origin, String destination, int availablePlaces, boolean smoking, boolean pets,
                         boolean luggage) {
        this.carModel = carModel;
        this.message = message;
        this.departureTime = departureTime;
        this.origin = origin;
        this.destination = destination;
        this.availablePlaces = availablePlaces;
        this.smoking = smoking;
        this.pets = pets;
        this.luggage = luggage;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isPets() {
        return pets;
    }

    public void setPets(boolean pets) {
        this.pets = pets;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }
}

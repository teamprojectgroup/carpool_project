package com.shipcars.finalproject.models;

import javax.persistence.Column;
import javax.validation.constraints.*;

public class CreateUserDTO {

    @Pattern(regexp = "^[a-zA-Z0-9_.]{1,40}$", message = "Username should be between 1 and 40 alphanumeric characters.")
    private String username;

    @Size(min = 4, max = 25, message = "First name should be between 4 and 25!")
    @Column(name = "first_name")
    private String firstName;

    @Size(min = 4, max = 25, message = "Last name should be between 4 and 25!")
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Email(message = "Please provide a valid email address.")
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "Please provide a valid email address.",
            flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.UNICODE_CASE})
    private String email;

    @Size(min = 4, max = 20, message = "Password should be between 4 and 20 symbols!")
    @Column(name = "password")
    private String password;

    @Digits(integer = 15, fraction = 0)
    @Size(min = 8, max = 15, message = "Phone number should be between 8 and 15 digits!")
    @Column(name = "phone")
    private String phone;

    public CreateUserDTO() {
    }

    public CreateUserDTO(String username, String firstName, String lastName,
                         String email, String password, String phone) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}


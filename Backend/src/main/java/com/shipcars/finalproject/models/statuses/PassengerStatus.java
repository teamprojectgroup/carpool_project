package com.shipcars.finalproject.models.statuses;

public enum PassengerStatus {
    pending, accepted, rejected, canceled, absent
}

package com.shipcars.finalproject.models;

import com.shipcars.finalproject.models.statuses.PassengerStatus;

import javax.validation.constraints.*;

public class PassengerDTO {

    private Long id;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9_.]{1,40}$", message = "Username should be between 1 and 40 alphanumeric characters.")
    private String username;

    @Size(min = 4, max = 25, message = "First name should be between 4 and 25!")
    private String firstName;

    @Size(min = 4, max = 25, message = "Last name should be between 4 and 25!")
    private String lastName;

    @NotNull
    @Email(message = "Please provide a valid email address.")
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "Please provide a valid email address.",
            flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.UNICODE_CASE})
    private String email;

    @Digits(integer = 15, fraction = 0)
    @Size(min = 8, max = 15, message = "Phone number should be between 8 and 15 digits!")
    private String phone;

    private double ratingAsPassenger;

    private PassengerStatus status;

    public PassengerDTO() {
    }

    public PassengerDTO(Long id, String username, String firstName, String lastName, String email, String phone,
                        double ratingAsPassenger, PassengerStatus status) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.ratingAsPassenger = ratingAsPassenger;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getRatingAsPassenger() {
        return ratingAsPassenger;
    }

    public void setRatingAsPassenger(double ratingAsPassenger) {
        this.ratingAsPassenger = ratingAsPassenger;
    }

    public PassengerStatus getStatus() {
        return status;
    }

    public void setStatus(PassengerStatus status) {
        this.status = status;
    }
}

package com.shipcars.finalproject.models;

import com.shipcars.finalproject.models.statuses.TripStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

public class TripDTO {
    private long id;

    @NotNull
    private UserDTO driver;

    @Size(min = 3, max = 50, message = "Car model should be between 3 and 50!")
    private String carModel;

    @Size(max = 255, message = "Message cannot be more than 255 symbols!")
    private String message;

    private LocalDateTime departureTime;

    @Size(min = 3, max = 50, message = "Origin should be between 3 and 50!")
    private String origin;

    @Size(min = 3, max = 50, message = "Destination should be between 3 and 50!")
    private String destination;

    @NotNull
    @Min(1)
    @Max(10)
    private int availablePlaces;

    private List<PassengerDTO> passengersList;

    @Enumerated(EnumType.STRING)
    private TripStatus status;

    private List<CommentDTO> comments;

    private boolean smoking;

    private boolean pets;

    private boolean luggage;

    public TripDTO() {
    }

    public TripDTO(long id, String carModel,
                   String message,
                   LocalDateTime departureTime,
                   String origin, String destination,
                   int availablePlaces, List<PassengerDTO> passengersList,
                   TripStatus status, List<CommentDTO> comments, boolean smoking, boolean pets, boolean luggage) {
        this.id = id;
        this.carModel = carModel;
        this.message = message;
        this.departureTime = departureTime;
        this.origin = origin;
        this.destination = destination;
        this.availablePlaces = availablePlaces;
        this.passengersList = passengersList;
        this.status = status;
        this.comments = comments;
        this.smoking = smoking;
        this.pets = pets;
        this.luggage = luggage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDTO getDriver() {
        return driver;
    }

    public void setDriver(UserDTO driver) {
        this.driver = driver;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public List<PassengerDTO> getPassengersList() {
        return passengersList;
    }

    public void setPassengersList(List<PassengerDTO> passengersList) {
        this.passengersList = passengersList;
    }

    public TripStatus getStatus() {
        return status;
    }

    public void setStatus(TripStatus status) {
        this.status = status;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isPets() {
        return pets;
    }

    public void setPets(boolean pets) {
        this.pets = pets;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }
}
package com.shipcars.finalproject.models;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

public class EditTripDTO {
    private long id;

    @Size(min = 3, max = 50, message = "Car model should be between 3 and 50!")
    private String carModel;

    @Size(max = 255, message = "Message cannot be more than 255 symbols!")
    private String message;

    private LocalDateTime departureTime;

    @Size(min = 3, max = 50, message = "Origin should be between 3 and 50!")
    private String origin;

    @Size(min = 3, max = 50, message = "Destination should be between 3 and 50!")
    private String destination;

    @NotNull
    @Min(1)
    @Max(10)
    private int availablePlaces;

    private boolean smoking;

    private boolean pets;

    private boolean luggage;

    public EditTripDTO() {
    }

    public EditTripDTO(long id, String carModel, String message, LocalDateTime departureTime, String origin,
                       String destination, int availablePlaces, boolean smoking, boolean pets, boolean luggage) {
        this.id = id;
        this.carModel = carModel;
        this.message = message;
        this.departureTime = departureTime;
        this.origin = origin;
        this.destination = destination;
        this.availablePlaces = availablePlaces;
        this.smoking = smoking;
        this.pets = pets;
        this.luggage = luggage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isPets() {
        return pets;
    }

    public void setPets(boolean pets) {
        this.pets = pets;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }
}


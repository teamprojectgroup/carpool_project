package com.shipcars.finalproject.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateCommentDTO {

    @Size(max = 255, message = "Comments cannot be longer than 255 characters!")
    private String message;

    @NotNull
    private UserDTO author;

    public CreateCommentDTO() {
    }

    public CreateCommentDTO(String message, UserDTO author) {
        this.message = message;
        this.author = author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserDTO author) {
        this.author = author;
    }
}

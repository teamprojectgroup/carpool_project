package com.shipcars.finalproject.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentDTO {
    private long id;

    @Size(max = 255, message = "Comments cannot be longer than 255 characters!")
    private String message;

    @NotNull
    private UserDTO author;

    public CommentDTO() {
    }

    public CommentDTO(long id, String message, UserDTO author) {
        this.id = id;
        this.message = message;
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDTO getAuthor() {
        return author;
    }

    public void setAuthor(UserDTO author) {
        this.author = author;
    }
}

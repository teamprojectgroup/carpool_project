package com.shipcars.finalproject.repositories;

import com.shipcars.finalproject.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}

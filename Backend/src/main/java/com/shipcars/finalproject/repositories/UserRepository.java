package com.shipcars.finalproject.repositories;

import com.shipcars.finalproject.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findAllByEmail(String email);

    List<User> findAllByUsernameContaining(String username);

    User findByUsername(String username);

    User findUserById(long userId);

    @Modifying
    @Transactional
    @Query(value = "update users set image_id = :imageId where id = :userId", nativeQuery = true)
    void updateImageId(@Param("userId") Long userId, @Param("imageId") int imageId);
}

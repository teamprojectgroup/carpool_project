package com.shipcars.finalproject.repositories;


import com.shipcars.finalproject.entities.UserTripRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserRatingRepository extends JpaRepository<UserTripRating, Long> {

    @Transactional
    @Query(value = "select avg(ur.rating) from user_ratings ur where ur.trip_role =:tripRole", nativeQuery = true)
    Double getAverageRatingByTripRole(@Param("tripRole") String tripRole);

    UserTripRating findByUserIdAndTripIdAndRatedBy(long userId, long tripId, long ratedBy);
}
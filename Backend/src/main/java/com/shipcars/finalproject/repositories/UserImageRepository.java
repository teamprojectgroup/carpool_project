package com.shipcars.finalproject.repositories;

import com.shipcars.finalproject.entities.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserImageRepository extends JpaRepository<UserImage, Integer> {
    UserImage getUserImageByUserId(long userId);
}

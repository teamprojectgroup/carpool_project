package com.shipcars.finalproject.repositories;

import com.shipcars.finalproject.entities.Trip;
import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.models.statuses.TripStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface TripRepository extends JpaRepository<Trip, Long> {

    @Transactional
    @Query(value = "select t.driver from trips t", nativeQuery = true)
    Set<Long> getDrivers();

    Page<Trip> findAllByStatusInAndDriverInAndOriginContainingAndDestinationContainingAndDepartureTimeBetweenAndAvailablePlacesInAndSmokingInAndPetsInAndLuggageIn(
            List<TripStatus> tripStatuses,
            List<User> driver,
            String origin,
            String destination,
            LocalDateTime earliest,
            LocalDateTime latest,
            List<Integer> availablePlaces,
            List<Boolean> smokingAllowed,
            List<Boolean> petsAllowed,
            List<Boolean> luggageAllowed,
            Pageable pageable
    );
}

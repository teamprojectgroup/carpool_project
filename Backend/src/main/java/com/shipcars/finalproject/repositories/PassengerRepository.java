package com.shipcars.finalproject.repositories;

import com.shipcars.finalproject.entities.Passenger;
import com.shipcars.finalproject.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {

    @Transactional
    void deleteAllByTripId(long tripId);

    Passenger findAllByTripIdAndUser(long tripId, User user);
}

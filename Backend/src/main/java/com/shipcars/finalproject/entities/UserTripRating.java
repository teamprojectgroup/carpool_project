package com.shipcars.finalproject.entities;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_ratings")
public class UserTripRating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_rating_id")
    private long id;

    @Column(name = "user_id")
    @NotNull
    private long userId;

    @Column(name = "rating")
    @Min(1)
    @Max(5)
    private Integer rating;

    @Column(name = "trip_role")
    private String tripRole;

    @Column(name = "trip_id")
    @NotNull
    private long tripId;

    @Column(name = "rated_by")
    @NotNull
    private long ratedBy;

    public UserTripRating() {
    }

    public UserTripRating(long userId, Integer rating, String tripRole, long tripId, long ratedBy) {
        this.userId = userId;
        this.rating = rating;
        this.tripRole = tripRole;
        this.tripId = tripId;
        this.ratedBy = ratedBy;
    }

    public UserTripRating(long id, long userId, Integer rating, String tripRole, long tripId, long ratedBy) {
        this.id = id;
        this.userId = userId;
        this.rating = rating;
        this.tripRole = tripRole;
        this.tripId = tripId;
        this.ratedBy = ratedBy;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getTripRole() {
        return tripRole;
    }

    public void setTripRole(String tripRole) {
        this.tripRole = tripRole;
    }

    public long getRatedBy() {
        return ratedBy;
    }

    public void setRatedBy(long ratedBy) {
        this.ratedBy = ratedBy;
    }
}


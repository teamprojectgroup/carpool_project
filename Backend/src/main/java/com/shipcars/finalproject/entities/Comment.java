package com.shipcars.finalproject.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "message")
    @Size(max = 255, message = "Message cannot be more than 255 symbols!")
    private String message;

    @Column(name = "trip_id")
    private long tripId;

    @OneToOne
    @JoinColumn(name = "author", referencedColumnName = "id")
    private User author;

    public Comment() {
    }

    public Comment(String message, long tripId, User author) {
        this.message = message;
        this.tripId = tripId;
        this.author = author;
    }

    public Comment(long id, String message, long tripId, User author) {
        this.id = id;
        this.message = message;
        this.tripId = tripId;
        this.author = author;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }
}

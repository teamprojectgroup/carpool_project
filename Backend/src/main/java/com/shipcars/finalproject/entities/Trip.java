package com.shipcars.finalproject.entities;

import com.shipcars.finalproject.models.statuses.TripStatus;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "trips")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trip_id")
    private long id;

    @OneToOne
    @JoinColumn(name = "driver")
    private User driver;

    @Size(min = 3, max = 50, message = "Car model should be between 3 and 50!")
    @Column(name = "car_model")
    private String carModel;

    @Column(name = "message")
    @Size(max = 255, message = "Message cannot be more than 255 symbols!")
    private String message;

    //  "departureTime": "2019-07-19T14:24:56.525Z",
    @Column(name = "departure_time")
    private LocalDateTime departureTime;

    @Column(name = "origin")
    @Size(min = 3, max = 50, message = "Origin should be between 3 and 50!")
    private String origin;

    @Column(name = "destination")
    @Size(min = 3, max = 50, message = "Destination should be between 3 and 50!")
    private String destination;

    @Column(name = "available_places")
    @NotNull
    @Min(1)
    @Max(10)
    private int availablePlaces;

    @OneToMany(mappedBy = "tripId")
    private List<Passenger> passengersList;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TripStatus status;

    @OneToMany(mappedBy = "tripId")
    private List<Comment> comments;

    @Column(name = "smoking")
    private boolean smoking;

    @Column(name = "pets")
    private boolean pets;

    @Column(name = "luggage")
    private boolean luggage;

    public Trip() {
    }

    public Trip(long id,
                User driver,
                String carModel,
                String message, LocalDateTime departureTime,
                String origin,
                String destination,
                int availablePlaces,
                List<Passenger> passengersList,
                TripStatus status,
                List<Comment> comments,
                boolean smoking,
                boolean pets,
                boolean luggage) {
        this.id = id;
        this.driver = driver;
        this.carModel = carModel;
        this.message = message;
        this.departureTime = departureTime;
        this.origin = origin;
        this.destination = destination;
        this.availablePlaces = availablePlaces;
        this.passengersList = passengersList;
        this.status = status;
        this.comments = comments;
        this.smoking = smoking;
        this.pets = pets;
        this.luggage = luggage;
    }

    public Trip(User driver,
                String carModel, String message, LocalDateTime departureTime, String origin, String destination,
                int availablePlaces, List<Passenger> passengersList, TripStatus status, List<Comment> comments,
                boolean smoking, boolean pets, boolean luggage) {
        this.driver = driver;
        this.carModel = carModel;
        this.message = message;
        this.departureTime = departureTime;
        this.origin = origin;
        this.destination = destination;
        this.availablePlaces = availablePlaces;
        this.passengersList = passengersList;
        this.status = status;
        this.comments = comments;
        this.smoking = smoking;
        this.pets = pets;
        this.luggage = luggage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public List<Passenger> getPassengersList() {
        return passengersList;
    }

    public void setPassengersList(List<Passenger> passengersList) {
        this.passengersList = passengersList;
    }

    public TripStatus getStatus() {
        return status;
    }

    public void setStatus(TripStatus status) {
        this.status = status;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isPets() {
        return pets;
    }

    public void setPets(boolean pets) {
        this.pets = pets;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }
}
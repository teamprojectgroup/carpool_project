package com.shipcars.finalproject.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_images")
public class UserImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    @NotNull
    private int id;

    @Column(name = "file_name")
    @NotNull
    @Size(min = 5, max = 100, message = "File name should be between 5 and 100 characters.")
    private String fileName;

    @Column(name = "user_id")
    @NotNull
    private long userId;

    @Column(name = "type")
    @NotNull
    @Size(min = 5, max = 15, message = "File extension should be between 5 and 15 characters.")
    private String fileType;

    @Column(name = "image")
    private byte[] image;

    public UserImage() {
    }

    public UserImage(String fileName, long userId, String fileType, byte[] image) {
        this.fileName = fileName;
        this.userId = userId;
        this.fileType = fileType;
        this.image = image;
    }

    public UserImage(int id, long userId, String username, String fileType, byte[] image) {
        this.id = id;
        this.fileName = fileName;
        this.userId = userId;
        this.fileType = fileType;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
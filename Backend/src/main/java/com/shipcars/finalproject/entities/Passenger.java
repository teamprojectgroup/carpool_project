package com.shipcars.finalproject.entities;

import com.shipcars.finalproject.models.statuses.PassengerStatus;

import javax.persistence.*;

@Entity
@Table(name = "passengers")
public class Passenger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "trip_id")
    private long tripId;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "passenger_status")
    @Enumerated(EnumType.STRING)
    private PassengerStatus status;

    public Passenger() {
    }

    public Passenger(long tripId, User user, PassengerStatus status) {
        this.tripId = tripId;
        this.user = user;
        this.status = status;
    }

    public Passenger(long id, long tripId, User user, PassengerStatus status) {
        this.id = id;
        this.tripId = tripId;
        this.user = user;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTripId() {
        return tripId;
    }

    public void setTripId(long tripId) {
        this.tripId = tripId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public PassengerStatus getStatus() {
        return status;
    }

    public void setStatus(PassengerStatus status) {
        this.status = status;
    }
}

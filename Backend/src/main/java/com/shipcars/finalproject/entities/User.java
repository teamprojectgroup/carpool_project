package com.shipcars.finalproject.entities;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9_.]{1,40}$", message = "Username should be between 1 and 40 alphanumeric characters.")
    private String username;

    @Size(min = 4, max = 25, message = "First name should be between 4 and 25!")
    @Column(name = "first_name")
    private String firstName;

    @Size(min = 4, max = 25, message = "Last name should be between 4 and 25!")
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Email(message = "Please provide a valid email address.")
    @Pattern(regexp = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", message = "Please provide a valid email address.",
            flags = {Pattern.Flag.CASE_INSENSITIVE, Pattern.Flag.UNICODE_CASE})
    private String email;

    @Size(min = 4, max = 255, message = "Password should be between 4 and 20 symbols!")
    @Column(name = "password")
    private String password;

    @Digits(integer = 15, fraction = 0)
    @Size(min = 8, max = 15, message = "Phone number should be between 8 and 15 digits!")
    @Column(name = "phone")
    private String phone;

    @Column(name = "rating_as_driver")
    private double ratingAsDriver;

    @Column(name = "rating_as_passenger")
    private double ratingAsPassenger;

    @OneToOne
    @JoinColumn(name = "image_id")
    private UserImage image;

    @ElementCollection(fetch = FetchType.EAGER)
    List<Role> roles;


    public User() {
    }

    public User(String username, String firstName, String lastName,
                String email, String password, String phone,
                double ratingAsDriver, double ratingAsPassenger, UserImage image, List<Role> roles) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.ratingAsDriver = ratingAsDriver;
        this.ratingAsPassenger = ratingAsPassenger;
        this.image = image;
        this.roles = roles;
    }

    public User(long id, String username, String firstName, String lastName,
                String email, String password, String phone,
                double ratingAsDriver, double ratingAsPassenger, UserImage image, List<Role> roles) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.ratingAsDriver = ratingAsDriver;
        this.ratingAsPassenger = ratingAsPassenger;
        this.image = image;
        this.roles = roles;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getRatingAsDriver() {
        return ratingAsDriver;
    }

    public void setRatingAsDriver(double ratingAsDriver) {
        this.ratingAsDriver = ratingAsDriver;
    }

    public double getRatingAsPassenger() {
        return ratingAsPassenger;
    }

    public void setRatingAsPassenger(double ratingAsPassenger) {
        this.ratingAsPassenger = ratingAsPassenger;
    }

    public UserImage getImage() {
        return image;
    }

    public void setImage(UserImage image) {
        this.image = image;
    }
}
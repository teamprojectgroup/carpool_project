package com.shipcars.finalproject.controllers;

import com.shipcars.finalproject.entities.Role;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.services.AdminService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/admins")
public class AdminRestController {

    private static final Logger logger = LoggerFactory.getLogger(AdminRestController.class);
    private AdminService adminService;

    @Autowired
    public AdminRestController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/users/{userID}")
    public void deleteUser(@PathVariable long userID) {
        try {
            adminService.deleteUser(userID);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/trips/{tripID}")
    public void deleteTrip(@PathVariable long tripID) {
        try {
            adminService.deleteTrip(tripID);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @PatchMapping("/users/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    ResponseEntity<Void> editUserRole(@PathVariable String username,
                                      @RequestParam(value = "role", required = true) Role role) {
        try {
            adminService.editUserRole(username, role);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}

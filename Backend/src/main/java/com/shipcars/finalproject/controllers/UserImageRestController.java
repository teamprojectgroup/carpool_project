package com.shipcars.finalproject.controllers;

import com.shipcars.finalproject.entities.UserImage;
import com.shipcars.finalproject.services.UserImageService;
import com.shipcars.finalproject.services.UserImageServiceImpl;
import com.shipcars.finalproject.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Base64;

@RestController
@RequestMapping("/users")
public class UserImageRestController extends FileValidator {
    private static final String FILE_COULD_NOT_BE_STORED = "File could not be stored. Please try again later!";

    private static final Logger logger = LoggerFactory.getLogger(UserImageRestController.class);
    private UserImageService userImageService;
    private UserService userService;

    @Autowired
    public UserImageRestController(UserImageService userImageService, UserService userService) {
        this.userImageService = userImageService;
        this.userService = userService;
    }

    @PostMapping("/{userId}/avatar")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<Void> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String userId) {
        validateFileAndName(file, "(.jpg|.jpeg|.png|.gif|.bmp)");
        UserImage image;

        try {
            image = new UserImage(file.getOriginalFilename(), Long.parseLong(userId), file.getContentType(),
                    file.getBytes());
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.CONFLICT, FILE_COULD_NOT_BE_STORED);
        }

        try {
            userImageService.storeFile(image, userId);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{userId}/avatar")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<String> get(@PathVariable String userId) {
        String image = Base64.getEncoder().encodeToString(userService.getUserImageByUserId(userId).getImage());
        try {
            return ResponseEntity.ok().body(image);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
package com.shipcars.finalproject.controllers;

import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.*;
import com.shipcars.finalproject.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserRestController {

    private static final Logger logger = LoggerFactory.getLogger(UserRestController.class);

    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody CreateUserDTO createUserDTO) {
        try {
            return ResponseEntity.ok().body(userService.createUser(createUserDTO));
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @PostMapping(value = "/login")
    ResponseEntity<JWTToken> login(@Valid @RequestBody LoginDTO loginVM) {
        try {
            return ResponseEntity.ok().body(userService.authorize(loginVM));
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @GetMapping(value = "/me")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public UserResponseDTO getCurrentUser(HttpServletRequest req) {
        try {
            return userService.getCurrentUser(req);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @GetMapping("/refresh")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public String refresh(HttpServletRequest req) {
        return userService.refresh(req.getRemoteUser());
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    ResponseEntity<EditUserDTO> editUser(@RequestBody EditUserDTO editUserDTO, HttpServletRequest req) {
        return ResponseEntity.ok().body(userService.editUser(editUserDTO, req));
    }

    @GetMapping("/{username}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity<UserDTO> getUserByUsername(@PathVariable String username) {
        try {
            return ResponseEntity.ok().body(userService.getUserByUsername(username));
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
    public ResponseEntity getAllUsers(@RequestParam(value = "username", required = false) String username) {
        try {
            List<User> users = userService.getAllUsers(username);
            return ResponseEntity.ok().body(users.stream());
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }
}

package com.shipcars.finalproject.controllers;

import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.*;
import com.shipcars.finalproject.models.statuses.PassengerStatus;
import com.shipcars.finalproject.models.statuses.TripStatus;
import com.shipcars.finalproject.services.TripRepresentationService;
import com.shipcars.finalproject.services.TripService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping("/trips")

public class TripRestController {

    private static final Logger logger = LoggerFactory.getLogger(TripRestController.class);

    private TripService tripService;
    private TripRepresentationService representationService;

    @Autowired
    public TripRestController(TripService tripService, TripRepresentationService representationService) {
        this.tripService = tripService;
        this.representationService = representationService;
    }

    @PostMapping
    public ResponseEntity<Void> createTrip(@Valid @RequestBody CreateTripDTO createTripDTO, HttpServletRequest req) {
        tripService.createTrip(createTripDTO, req);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public ResponseEntity getTrips(@RequestParam(value = "status", required = false) TripStatus status,
                                   @RequestParam(value = "driver", required = false) String driver,
                                   @RequestParam(value = "origin", required = false) String origin,
                                   @RequestParam(value = "destination", required = false) String destination,
                                   @RequestParam(value = "earliestDepartureTime",
                                           required = false) String earliestDepartureTime,
                                   @RequestParam(value = "latestDepartureTime", required =
                                           false) String latestDepartureTime,
                                   @RequestParam(value = "availablePlaces", required =
                                           false) @Min(1) @Max(10) Integer availablePlaces,
                                   @RequestParam(value = "smoking", required = false) Boolean smoking,
                                   @RequestParam(value = "pets", required = false) Boolean pets,
                                   @RequestParam(value = "luggage", required = false) Boolean luggage,
                                   Pageable pageable) {

        try {
            Page<TripDTO> trips = representationService.getTrips(
                    status, driver, origin, destination, earliestDepartureTime, latestDepartureTime, availablePlaces,
                    smoking, pets, luggage, pageable);
            return ResponseEntity.ok().body(trips);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @PostMapping("{id}/comments")
    ResponseEntity<Void> addComment(@NotNull @PathVariable("id") Long id,
                                    @RequestBody CreateCommentDTO comment, HttpServletRequest request) {
        try {
            tripService.addComment(id, comment, request);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @PostMapping("/{id}/passengers")
    ResponseEntity<Void> apply(@PathVariable("id") long id, HttpServletRequest req) {
        try {
            tripService.addPassenger(id, req);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PatchMapping("/{tripId}/passengers/{passengerId}")
    ResponseEntity<Void> changePassengerStatus(@PathVariable("tripId") long tripId,
                                               @PathVariable("passengerId") long passengerId,
                                               @RequestParam PassengerStatus status) {

        try {
            tripService.changePassengerStatus(tripId, passengerId, status);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping("/{id}")
    ResponseEntity<Void> changeTripStatus(@PathVariable("id") long id,
                                          @RequestParam(value = "status", required = true) TripStatus status,
                                          HttpServletRequest request) {
        try {
            tripService.changeTripStatus(id, status, request);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @PutMapping
    ResponseEntity<Void> editTrip(@Valid @RequestBody EditTripDTO editTripDTO, HttpServletRequest req) {
        tripService.editTrip(editTripDTO, req);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @GetMapping("/{id}")
    ResponseEntity<TripDTO> getTrip(@PathVariable("id") long id) {
        try {
            return ResponseEntity.ok().body(representationService.getTrip(id));
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
    }

    @PostMapping("/{id}/driver/rate/{rating}")
    ResponseEntity<Void> rateDriver(@PathVariable("id") long id, @Min(1) @Max(5) @PathVariable Integer rating,
                                    HttpServletRequest req) {
        try {
            tripService.rateDriver(id, rating, req);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PostMapping("/{tripId}/passengers/{passengerId}/rate/{rating}")
    ResponseEntity<Void> ratePassenger(@NotNull @PathVariable("tripId") Long tripId,
                                       @NotNull @PathVariable("passengerId") Long passengerId,
                                       @Min(1) @Max(5) @PathVariable Integer rating, HttpServletRequest req) {

        try {
            tripService.ratePassenger(tripId, passengerId, rating, req);
        } catch (CustomException e) {
            logger.error(e.getMessage());
            throw new ResponseStatusException(e.getHttpStatus(), e.getMessage());
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @GetMapping("/allPassengerStatuses")
    ResponseEntity<List<PassengerStatus>> allPassengerStatuses() {
        List<PassengerStatus> statuses;
        statuses = tripService.allPassengerStatuses();

        return ResponseEntity.ok().body(statuses);
    }

    @GetMapping("/allTripStatuses")
    ResponseEntity<List<TripStatus>> allTripStatuses() {
        List<TripStatus> statuses;
        statuses = tripService.allTripStatuses();

        return ResponseEntity.ok().body(statuses);
    }
}

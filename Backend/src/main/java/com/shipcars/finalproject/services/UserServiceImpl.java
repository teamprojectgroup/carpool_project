package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.Role;
import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.entities.UserImage;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.*;
import com.shipcars.finalproject.repositories.UserRepository;
import com.shipcars.finalproject.security.JwtTokenProvider;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private ModelMapper modelMapper;
    private JwtTokenProvider jwtTokenProvider;
    private AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           ModelMapper modelMapper,
                           JwtTokenProvider jwtTokenProvider,
                           PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public UserDTO createUser(CreateUserDTO createUserDTO) {

        if (createUserDTO == null) {
            throw new CustomException("User does not exist!", HttpStatus.NOT_FOUND);
        }

        User user = userRepository.findAllByEmail(createUserDTO.getEmail());
        if (user != null) {
            throw new CustomException("User already exists!", HttpStatus.CONFLICT);
        }

        String emailPrefix = createUserDTO.getEmail().split("@")[0];
        List<User> users = userRepository.findAllByUsernameContaining(emailPrefix);

        String username;
        if (users.size() > 0) {
            username = emailPrefix + (users.size() + 1);
        } else {
            username = emailPrefix;
        }
        createUserDTO.setUsername(username);

        createUserDTO.setPassword(passwordEncoder.encode(createUserDTO.getPassword()));

        user = modelMapper.map(createUserDTO, User.class);
        user.setRoles(new ArrayList<>(Arrays.asList(Role.ROLE_USER)));
        userRepository.save(user);
        return modelMapper.map(user, UserDTO.class);
    }

    @Override
    public JWTToken authorize(LoginDTO loginVM) {
        User user = userRepository.findAllByEmail(loginVM.getEmail());

        if (user == null) {
            throw new CustomException("User doesn't exist!", HttpStatus.CONFLICT);
        }

        String username = user.getUsername();

        try {
            authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username,
                            loginVM.getPassword()));
            JWTToken token = new JWTToken(jwtTokenProvider.createToken(username, user.getRoles()));
            return token;

        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @Override
    public UserResponseDTO getCurrentUser(HttpServletRequest req) {
        return modelMapper.map(userRepository.findByUsername(jwtTokenProvider
                .getUsername(jwtTokenProvider.resolveToken(req))), UserResponseDTO.class);
    }

    public String refresh(String username) {
        return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
    }

    @Override
    public EditUserDTO editUser(EditUserDTO editUserDTO, HttpServletRequest req) {
        User userToUpdate =
                userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
        if (userToUpdate == null) {
            throw new CustomException("User does not exist!", HttpStatus.NOT_FOUND);
        }

        userToUpdate.setFirstName(editUserDTO.getFirstName());
        userToUpdate.setLastName(editUserDTO.getLastName());
        userToUpdate.setEmail(editUserDTO.getEmail());
        userToUpdate.setPhone(editUserDTO.getPhone());
        userRepository.save(userToUpdate);
        return modelMapper.map(userToUpdate, EditUserDTO.class);
    }

    @Override
    public UserDTO getUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (username == null || user == null) {
            throw new CustomException("Username is not found!", HttpStatus.NOT_FOUND);
        }

        return modelMapper.map(user, UserDTO.class);
    }

    @Override
    public List<User> getAllUsers(String username) {
        if (username != null) {
            List<User> users = userRepository.findAllByUsernameContaining(username);
            return users;
        }
        return userRepository.findAll();
    }

    @Override
    public UserImage getUserImageByUserId(String userId) {
        return userRepository.findUserById(Long.parseLong(userId)).getImage();
    }
}
package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.entities.UserImage;
import com.shipcars.finalproject.models.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {
    UserDTO createUser(CreateUserDTO createUserDTO);

    JWTToken authorize(LoginDTO loginVM);

    UserResponseDTO getCurrentUser(HttpServletRequest req);

    String refresh(String remoteUser);

    EditUserDTO editUser(EditUserDTO editUserDTO, HttpServletRequest req);

    UserDTO getUserByUsername(String username);

    List<User> getAllUsers(String username);

    UserImage getUserImageByUserId(String userId);
}
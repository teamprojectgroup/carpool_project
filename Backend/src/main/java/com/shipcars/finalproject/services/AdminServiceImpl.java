package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.Role;
import com.shipcars.finalproject.entities.Trip;
import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.repositories.TripRepository;
import com.shipcars.finalproject.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    private UserRepository userRepository;
    private TripRepository tripRepository;

    @Autowired
    public AdminServiceImpl(UserRepository userRepository, TripRepository tripRepository) {
        this.userRepository = userRepository;
        this.tripRepository = tripRepository;
    }

    @Override
    public void deleteUser(long userID) {
        User user = userRepository.findUserById(userID);
        if (user == null) {
            throw new CustomException("User is not found!", HttpStatus.NOT_FOUND);
        }
        userRepository.delete(user);
    }

    @Override
    public void deleteTrip(long tripID) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(tripID));
        if (trips.size() != 1) {
            throw new CustomException("Trip does not exist!", HttpStatus.NOT_FOUND);
        }
        Trip trip = trips.get(0);
        tripRepository.delete(trip);
    }

    @Override
    public void editUserRole(String username, Role role) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new CustomException("User does not exist!", HttpStatus.NOT_FOUND);
        }

        user.setRoles(new ArrayList<>(Arrays.asList(role)));
        userRepository.save(user);
    }
}

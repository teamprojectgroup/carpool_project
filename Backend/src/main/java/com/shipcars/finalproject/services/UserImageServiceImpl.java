package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.entities.UserImage;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.repositories.UserImageRepository;
import com.shipcars.finalproject.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserImageServiceImpl implements UserImageService {
    private UserImageRepository userImageRepository;
    private UserRepository userRepository;

    @Autowired
    public UserImageServiceImpl(UserImageRepository userImageRepository, UserRepository userRepository) {
        this.userImageRepository = userImageRepository;
        this.userRepository = userRepository;
    }

    public UserImage storeFile(UserImage image, String userId) {
        if (userId == null || !userRepository.findById(Long.parseLong(userId)).isPresent()) {
            throw new CustomException("User not found!", HttpStatus.NOT_FOUND);
        }

        userImageRepository.save(image);
        userRepository.updateImageId(Long.parseLong(userId), image.getId());
        return image;
    }

    public UserImage getFile(String userId) {
        Optional<User> user = userRepository.findById(Long.parseLong(userId));
        if (userId == null || !user.isPresent()) {
            throw new CustomException("User not found!", HttpStatus.NOT_FOUND);
        }
        return userImageRepository.getUserImageByUserId(Long.parseLong(userId));
    }
}

package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.Comment;
import com.shipcars.finalproject.entities.Passenger;
import com.shipcars.finalproject.entities.Trip;
import com.shipcars.finalproject.entities.User;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.*;
import com.shipcars.finalproject.models.statuses.TripStatus;
import com.shipcars.finalproject.repositories.TripRepository;
import com.shipcars.finalproject.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class TripRepresentationServiceImpl implements TripRepresentationService {
    private TripRepository tripRepository;
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    @Autowired
    public TripRepresentationServiceImpl(TripRepository tripRepository, UserRepository userRepository,
                                         ModelMapper modelMapper) {
        this.tripRepository = tripRepository;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Page<TripDTO> getTrips(TripStatus status, String driver, String origin, String destination,
                                  String earliestDepartureTime,
                                  String latestDepartureTime,
                                  Integer availablePlaces,
                                  Boolean smoking,
                                  Boolean pets,
                                  Boolean luggage,
                                  Pageable pageable) {

        List<TripStatus> tripStatuses = new ArrayList<>();
        if (status != null) {
            tripStatuses.add(status);
        } else {
            tripStatuses = Arrays.asList(TripStatus.values());
        }

        Set<Long> driversIds;
        List<User> drivers = new ArrayList<>();
        User user;
        if (driver == null) {
            driversIds = tripRepository.getDrivers();
            drivers = userRepository.findAllById(driversIds);
        } else {
            user = userRepository.findByUsername(driver);
            if (user == null) {
                throw new CustomException("User does not exist!", HttpStatus.NOT_FOUND);
            }
            drivers.add(user);
        }

        if (origin == null) {
            origin = "";
        }

        if (destination == null) {
            destination = "";
        }

        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        LocalDateTime earliest;
        LocalDateTime latest;
        if (earliestDepartureTime != null) {
            earliest = LocalDateTime.parse(earliestDepartureTime, formatter);
        } else {
            earliest = LocalDateTime.parse("1900-01-01T00:00:00", formatter);
        }

        if (latestDepartureTime != null) {
            latest = LocalDateTime.parse(latestDepartureTime, formatter);
        } else {
            latest = LocalDateTime.parse("2900-01-01T00:00:00", formatter);
        }

        List<Integer> places;
        if (availablePlaces == null) {
            places = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        } else {
            places = new ArrayList<>(Arrays.asList(availablePlaces));
        }

        List<Boolean> smokingAllowed;
        if (smoking == null) {
            smokingAllowed = new ArrayList<>(Arrays.asList(true, false));
        } else {
            smokingAllowed = new ArrayList<>(Arrays.asList(smoking));
        }

        List<Boolean> petsAllowed;
        if (pets == null) {
            petsAllowed = new ArrayList<>(Arrays.asList(true, false));
        } else {
            petsAllowed = new ArrayList<>(Arrays.asList(pets));
        }

        List<Boolean> luggageAllowed;
        if (luggage == null) {
            luggageAllowed = new ArrayList<>(Arrays.asList(true, false));
        } else {
            luggageAllowed = new ArrayList<>(Arrays.asList(luggage));
        }

        Page<Trip> tripList =
                tripRepository.findAllByStatusInAndDriverInAndOriginContainingAndDestinationContainingAndDepartureTimeBetweenAndAvailablePlacesInAndSmokingInAndPetsInAndLuggageIn(
                        tripStatuses,
                        drivers,
                        origin,
                        destination,
                        earliest,
                        latest,
                        places,
                        smokingAllowed,
                        petsAllowed,
                        luggageAllowed,
                        pageable
                );

        return tripList.map(this::convertToTripDTO);
    }

    @Override
    public TripDTO getTrip(long id) {

        List<Trip> trips = tripRepository.findAllById(Collections.singleton(id));
        if (trips.size() != 1) {
            throw new CustomException("There is no such trip", HttpStatus.NOT_FOUND);
        }
        TripDTO tripDTO = convertToTripDTO(trips.get(0));
        return tripDTO;
    }

    private TripDTO convertToTripDTO(Trip trip) {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setId(trip.getId());
        tripDTO.setDriver(modelMapper.map(trip.getDriver(), UserDTO.class));
        tripDTO.setCarModel(trip.getCarModel());
        tripDTO.setMessage(trip.getMessage());
        tripDTO.setDepartureTime(trip.getDepartureTime());
        tripDTO.setOrigin(trip.getOrigin());
        tripDTO.setDestination(trip.getDestination());
        tripDTO.setAvailablePlaces(trip.getAvailablePlaces());

        List<Passenger> passengersList = trip.getPassengersList();
        List<PassengerDTO> passengerDTOList = new ArrayList<>();
        PassengerDTO passengerDTO = new PassengerDTO();
        for (Passenger passenger : passengersList) {
            passengerDTO.setId(passenger.getUser().getId());
            passengerDTO.setUsername(passenger.getUser().getUsername());
            passengerDTO.setFirstName(passenger.getUser().getFirstName());
            passengerDTO.setLastName(passenger.getUser().getLastName());
            passengerDTO.setEmail(passenger.getUser().getEmail());
            passengerDTO.setPhone(passenger.getUser().getPhone());
            passengerDTO.setRatingAsPassenger(passenger.getUser().getRatingAsPassenger());
            passengerDTO.setStatus(passenger.getStatus());
            passengerDTOList.add(passengerDTO);
        }
        tripDTO.setPassengersList(passengerDTOList);

        tripDTO.setStatus(trip.getStatus());

        List<Comment> commentList = trip.getComments();
        List<CommentDTO> commentDTOList = new ArrayList<>();
        for (Comment comment : commentList) {
            CommentDTO commentDTO = new CommentDTO();
            commentDTO.setId(comment.getId());
            commentDTO.setMessage(comment.getMessage());

            UserDTO authorDTOS = modelMapper.map(comment.getAuthor(), UserDTO.class);
            commentDTO.setAuthor(authorDTOS);
            commentDTOList.add(commentDTO);
        }

        tripDTO.setComments(commentDTOList);

        tripDTO.setSmoking(trip.isSmoking());
        tripDTO.setPets(trip.isPets());
        tripDTO.setLuggage(trip.isLuggage());

        return tripDTO;
    }
}

package com.shipcars.finalproject.services;

import com.shipcars.finalproject.models.TripDTO;
import com.shipcars.finalproject.models.statuses.TripStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TripRepresentationService {

    Page<TripDTO> getTrips(TripStatus status, String driver, String origin, String destination,
                           String earliestDepartureTime,
                           String latestDepartureTime,
                           Integer availablePlaces,
                           Boolean smoking,
                           Boolean pets,
                           Boolean luggage,
                           Pageable pageable);

    TripDTO getTrip(long id);
}

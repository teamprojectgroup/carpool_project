package com.shipcars.finalproject.services;

import com.shipcars.finalproject.models.*;
import com.shipcars.finalproject.models.statuses.PassengerStatus;
import com.shipcars.finalproject.models.statuses.TripStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface TripService {
    void createTrip(CreateTripDTO createTripDTO, HttpServletRequest req);

    void addComment(Long id, CreateCommentDTO comment, HttpServletRequest req);

    void editTrip(EditTripDTO editTripDTO, HttpServletRequest req);

    void changeTripStatus(long id, TripStatus status, HttpServletRequest req);

    void addPassenger(long id, HttpServletRequest req);

    void rateDriver(Long tripId, Integer rating, HttpServletRequest req);

    void ratePassenger(Long tripId, Long passengerId, Integer rating, HttpServletRequest req);

    void changePassengerStatus(long tripId, long passengerId, PassengerStatus status);

    List<PassengerStatus> allPassengerStatuses();

    List<TripStatus> allTripStatuses();
}

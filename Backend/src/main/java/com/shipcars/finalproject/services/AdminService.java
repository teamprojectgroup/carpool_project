package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.Role;

public interface AdminService {
    void deleteUser(long userID);

    void deleteTrip(long tripID);

    void editUserRole(String username, Role role);
}

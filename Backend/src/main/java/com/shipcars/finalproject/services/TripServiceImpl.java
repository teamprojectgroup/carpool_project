package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.*;
import com.shipcars.finalproject.exceptions.CustomException;
import com.shipcars.finalproject.models.CreateCommentDTO;
import com.shipcars.finalproject.models.CreateTripDTO;
import com.shipcars.finalproject.models.EditTripDTO;
import com.shipcars.finalproject.models.statuses.PassengerStatus;
import com.shipcars.finalproject.models.statuses.TripStatus;
import com.shipcars.finalproject.repositories.*;
import com.shipcars.finalproject.security.JwtTokenProvider;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class TripServiceImpl implements TripService {
    TripRepository tripRepository;
    ModelMapper modelMapper;
    UserRepository userRepository;
    JwtTokenProvider tokenProvider;
    CommentRepository commentRepository;
    PassengerRepository passengerRepository;
    UserRatingRepository userRatingRepository;

    @Autowired
    public TripServiceImpl(TripRepository tripRepository, ModelMapper modelMapper, UserRepository userRepository,
                           JwtTokenProvider tokenProvider, CommentRepository commentRepository,
                           PassengerRepository passengerRepository, UserRatingRepository userRatingRepository
    ) {
        this.tripRepository = tripRepository;
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
        this.tokenProvider = tokenProvider;
        this.commentRepository = commentRepository;
        this.passengerRepository = passengerRepository;
        this.userRatingRepository = userRatingRepository;
    }

    @Override
    public void createTrip(CreateTripDTO createTripDTO, HttpServletRequest req) {
        Trip trip = modelMapper.map(createTripDTO, Trip.class);
        trip.setDriver(userRepository.findByUsername(tokenProvider.getUsername(tokenProvider.resolveToken(req))));
        trip.setStatus(TripStatus.available);
        tripRepository.save(trip);
    }

    @Override
    public void addComment(Long id, CreateCommentDTO comment, HttpServletRequest req) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(id));
        if (id == null || trips.size() != 1) {
            throw new CustomException("Trip not found!", HttpStatus.NOT_FOUND);
        }

        User user = userRepository.findByUsername(tokenProvider.getUsername(tokenProvider.resolveToken(req)));
        if (!user.getUsername().equalsIgnoreCase(comment.getAuthor().getUsername())) {
            throw new CustomException("You are not authorized to comment!",
                    HttpStatus.UNAUTHORIZED);
        }

        if (!trips.get(0).getDriver().getUsername().equalsIgnoreCase(user.getUsername()) &&
                passengerRepository.findAllByTripIdAndUser(id, user) == null) {
            throw new CustomException("You are not authorized to comment, because you are not on the trip!",
                    HttpStatus.UNAUTHORIZED);
        }

        Comment newComment = new Comment(comment.getMessage(), id, user);
        commentRepository.save(newComment);
    }

    @Override
    public void editTrip(EditTripDTO editTripDTO, HttpServletRequest req) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(editTripDTO.getId()));

        if (trips.size() != 1) {
            throw new CustomException("Trip not found!", HttpStatus.NOT_FOUND);
        }

        if (!trips.get(0).getDriver().getUsername().equalsIgnoreCase(tokenProvider.getUsername(tokenProvider.resolveToken(req)))) {
            throw new CustomException("You are not authorized to edit trip!", HttpStatus.UNAUTHORIZED);
        }

        if (editTripDTO.getAvailablePlaces() < trips.get(0).getPassengersList().size()) {
            throw new CustomException("There are not enough places on the trip!", HttpStatus.CONFLICT);
        }

        trips.get(0).setCarModel(editTripDTO.getCarModel());
        trips.get(0).setMessage(editTripDTO.getMessage());
        trips.get(0).setDepartureTime(editTripDTO.getDepartureTime());
        trips.get(0).setOrigin(editTripDTO.getOrigin());
        trips.get(0).setDestination(editTripDTO.getDestination());
        trips.get(0).setAvailablePlaces(editTripDTO.getAvailablePlaces());
        trips.get(0).setSmoking(editTripDTO.isSmoking());
        trips.get(0).setPets(editTripDTO.isPets());
        trips.get(0).setLuggage(editTripDTO.isLuggage());

        tripRepository.save(trips.get(0));
    }

    @Override
    public void changeTripStatus(long id, TripStatus status, HttpServletRequest req) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(id));
        if (trips.size() != 1) {
            throw new CustomException("Cannot change status of a non existing trip!", HttpStatus.NOT_FOUND);
        }

        if (!trips.get(0).getDriver().getUsername().equalsIgnoreCase(tokenProvider.getUsername(tokenProvider.resolveToken(req)))) {
            throw new CustomException("Only the driver can change the status of a trip!", HttpStatus.CONFLICT);
        }

        List<TripStatus> tripStatuses = Arrays.asList(TripStatus.values());
        int currentIndex = tripStatuses.indexOf(trips.get(0).getStatus());
        int newIndex = tripStatuses.indexOf(status);

        if (newIndex - currentIndex != 1 && status != TripStatus.canceled) {
            throw new CustomException("Status transition is not allowed!", HttpStatus.CONFLICT);
        }

        trips.get(0).setStatus(status);
        tripRepository.save(trips.get(0));
        if (status.equals(TripStatus.canceled)) {
            passengerRepository.deleteAllByTripId(id);
            tripRepository.deleteById(id);
        }
    }

    @Override
    public void changePassengerStatus(long tripId, long passengerId, PassengerStatus status) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(tripId));
        if (trips.size() != 1) {
            throw new CustomException("Cannot change status of a non existing trip!", HttpStatus.NOT_FOUND);
        }

        if (!trips.get(0).getPassengersList().contains(passengerRepository.findAllByTripIdAndUser(tripId,
                userRepository.findUserById(passengerId)))) {
            throw new CustomException("No such passenger on that trip!", HttpStatus.NOT_FOUND);
        }

        Passenger passenger = passengerRepository.findAllByTripIdAndUser(tripId,
                userRepository.findUserById(passengerId));

        if (status.equals(PassengerStatus.canceled) || status.equals(PassengerStatus.rejected)) {
            passengerRepository.delete(passenger);
        } else {
            passenger.setStatus(status);
            passengerRepository.save(passenger);
        }
    }

    @Override
    public List<PassengerStatus> allPassengerStatuses() {
        List<PassengerStatus> statuses = Arrays.asList(PassengerStatus.values());
        return statuses;
    }

    @Override
    public List<TripStatus> allTripStatuses() {
        List<TripStatus> statuses = Arrays.asList(TripStatus.values());
        return statuses;
    }

    @Override
    public void addPassenger(long id, HttpServletRequest req) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(id));
        if (trips.size() != 1) {
            throw new CustomException("Trip does not exist!", HttpStatus.NOT_FOUND);
        }

        if (trips.get(0).getDriver().getUsername().equalsIgnoreCase(tokenProvider.getUsername(tokenProvider.resolveToken(req)))) {
            throw new CustomException("Driver cannot apply as passenger for own trip!", HttpStatus.CONFLICT);
        }

        User user = userRepository.findByUsername(tokenProvider.getUsername(tokenProvider.resolveToken(req)));
        if (user == null) {
            throw new CustomException("User not found!", HttpStatus.NOT_FOUND);
        }

        if (passengerRepository.findAllByTripIdAndUser(id, user) != null) {
            throw new CustomException("You already applied for that trip!", HttpStatus.CONFLICT);
        }

        if (!trips.get(0).getStatus().equals(TripStatus.available)) {
            throw new CustomException("You cannot apply for that trip!", HttpStatus.CONFLICT);
        }

        if (trips.get(0).getAvailablePlaces() == trips.get(0).getPassengersList().size()) {
            throw new CustomException("No available places for this trip!", HttpStatus.CONFLICT);
        }

        Passenger passenger = new Passenger(id, user, PassengerStatus.pending);
        passengerRepository.save(passenger);
    }

    @Override
    public void rateDriver(Long tripId, Integer rating, HttpServletRequest req) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(tripId));
        if (trips.size() != 1) {
            throw new CustomException("Trip does not exist!", HttpStatus.NOT_FOUND);
        }

        if (!trips.get(0).getStatus().equals(TripStatus.done)) {
            throw new CustomException("You cannot rate participant in a trip, which is not done!", HttpStatus.CONFLICT);
        }

        User ratedBy = userRepository.findByUsername(tokenProvider.getUsername(tokenProvider.resolveToken(req)));
        UserTripRating userRatingRatedUser;

        Double averageRating;

        if (passengerRepository.findAllByTripIdAndUser(tripId, ratedBy) == null) {
            throw new CustomException("Yor are not authorized to rate that driver!", HttpStatus.CONFLICT);
        }

        userRatingRatedUser = userRatingRepository.findByUserIdAndTripIdAndRatedBy(trips.get(0).getDriver().getId(),
                tripId, ratedBy.getId());
        if (userRatingRatedUser == null) {
            userRatingRatedUser = new UserTripRating(trips.get(0).getDriver().getId(), rating, "driver", tripId,
                    ratedBy.getId());
        } else {
            userRatingRatedUser.setRating(rating);
        }
        userRatingRepository.save(userRatingRatedUser);
        averageRating = userRatingRepository.getAverageRatingByTripRole(userRatingRatedUser.getTripRole());
        trips.get(0).getDriver().setRatingAsDriver(averageRating);
        userRepository.save(trips.get(0).getDriver());
    }

    @Override
    public void ratePassenger(Long tripId, Long passengerId, Integer rating, HttpServletRequest req) {
        List<Trip> trips = tripRepository.findAllById(Collections.singleton(tripId));
        if (trips.size() != 1) {
            throw new CustomException("Trip does not exist!", HttpStatus.NOT_FOUND);
        }

        if (!trips.get(0).getStatus().equals(TripStatus.done)) {
            throw new CustomException("You cannot rate participant in a trip, which is not done !",
                    HttpStatus.CONFLICT);
        }

        User ratedBy = userRepository.findByUsername(tokenProvider.getUsername(tokenProvider.resolveToken(req)));
        UserTripRating userRatingRatedUser;

        Double averageRating;

        if (!trips.get(0).getDriver().getUsername().equalsIgnoreCase(ratedBy.getUsername())) {
            throw new CustomException("Yor are not authorized to rate the passengers of that trip!",
                    HttpStatus.CONFLICT);
        }

        List<User> passengers = userRepository.findAllById(Collections.singleton(passengerId));
        if (passengerRepository.findAllByTripIdAndUser(tripId, passengers.get(0)) == null) {
            throw new CustomException("Yor are not authorized to rate that passenger!", HttpStatus.CONFLICT);
        }

        userRatingRatedUser = userRatingRepository.findByUserIdAndTripIdAndRatedBy(passengerId, tripId,
                ratedBy.getId());
        if (userRatingRatedUser == null) {
            userRatingRatedUser = new UserTripRating(passengerId, rating, "passenger", tripId, ratedBy.getId());
        } else {
            userRatingRatedUser.setRating(rating);
        }

        userRatingRepository.save(userRatingRatedUser);
        averageRating = userRatingRepository.getAverageRatingByTripRole(userRatingRatedUser.getTripRole());
        passengers.get(0).setRatingAsPassenger(averageRating);
        userRepository.save(passengers.get(0));
    }
}


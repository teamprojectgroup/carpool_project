package com.shipcars.finalproject.services;

import com.shipcars.finalproject.entities.UserImage;

public interface UserImageService {

    UserImage storeFile(UserImage image, String userId);

    UserImage getFile(String userId);
}
